import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import java.util.ArrayList;
import org.json.simple.parser.JSONParser;
import java.io.FileReader;
import org.json.simple.JSONObject;
import java.io.FileWriter;
import java.io.IOException;

public class Tarea {
    public Tarea(Boolean vacio, String info, ArrayList<Tarea> lista, JSONObject JSON){
        
        id_dato++;  //Se aumenta en 1 el id estático de la clase
        id=id_dato; //Luego, este id se asigna a esta instancia en específico

        //Se inicializa Objeto JSONObject
        JSONTareas = new JSONObject(); 

        //Se guarda Objecto JSON recibido de constructor en atributo propio
        this.JSON = JSON;  

        //Se crea y prepara campo de texto
        campo = new TextField();
        campo.setPrefWidth(200);

        //Se crea y prepara Checkbox
        ch = new CheckBox();
        ch.setStyle("-fx-border-color: black;");

        //Se crea y prepara label

        texto = new Label();
        texto.setFont(new Font("Comic Sans MS", 20));
        texto.setPrefWidth(200);

        //Se crea un HBOX, que sera el panel que guardara los distintos objetos a ser desplegados en la interfaz (Checkbox, Label y TextField)
        tarea= new HBox();

        //Se inicializa el modo edit en falso (No se esta editanto los datos)
        editmode = false;

        if(vacio)  //En caso de que la Mascota no tenga datos
        {
            tarea.getChildren().addAll(campo,ch);  //Se agregaran al panel campo de texto y checkbox
            editmode = true;  //El modo edit se setea a verdadero (Se habilitó la edición de datos)
        }

        else  //Si la Mascota ya cuenta con información guardada (El modo edit se mantiene desactivado)
        {
            JSONParser jsonParser = new JSONParser();
            try (FileReader reader = new FileReader("./JSON/" + info + ".json")) {
                
                Object obj = jsonParser.parse(reader);  //Se extrae del .json de la Mascota en particular su información.

                JSONObject PetObject = (JSONObject) obj;

                //Del JSON, se obtiene cual es la Tarea en particular y si esta estaba marcada o no, dependiendo del id de la Tarea.
                JSONObject numTarea = (JSONObject) PetObject.get("Tarea" + id);
                String info_tarea = (String) numTarea.get("Tarea");
                Boolean checkbox = (Boolean) numTarea.get("Checkbox");

                //La información extraida se guarda en un objeto JSON de la instancia Tarea
                JSONTareas.put("Tarea", info_tarea);
                JSONTareas.put("Checkbox", checkbox);

                //La información extraida se coloca en el label texto y el checkbox ch
                texto.setText(info_tarea);
                ch.setSelected(checkbox);
            
                
                if(texto.getText()!=""){  //Si se extrajo información sobre la tarea, se añade al panel (se mostrará), de caso contrario no lo hará.
                    tarea.getChildren().addAll(texto,ch);
                    ch.setStyle("-fx-border-color: black;");
                }
            } catch (Exception e) {

            }
        }

        //Se añade esta instancia a la lista recibida en el constructor, la cual almacenará todas las intancias de Tareas creadas.
        lista.add(this);

        //Se define el handler para cuando se clickea la checkbox de la instancia de Tarea (Se actualiza el .json de la mascota)
        ch.setOnMouseClicked(e -> {

            if(!editmode)  //Solo se realizan cambios si no se encuentra en el modo edit
            {
                SaveCheck();  //Se llama a este metodo que se ocupa de actualizar la información guardada de la instancia en el JSON local y el global

                try (FileWriter archivo = new FileWriter("./JSON/" + info + ".json")) {
                    archivo.write(JSON.toJSONString());   //Se escribo en el archivo .json de la mascota la información actualizada en el JSON global.
                    archivo.flush();
            
                } catch (IOException eve) {
                    eve.printStackTrace();
                }
            }
        });
        
    }

    //Metodo que es llamado cuando se presiona el botón Guardar en el menú individual de la Mascota
    public void Save(JSONObject JSONG){
        editmode = false;  //Se desactiva el modo edit

        //Se extrae la informacion escrita en el campo de texto y el checkbox.
        String info_tarea = campo.getText();
        Boolean check = ch.isSelected();

        //Se quita el campo de texto y el checkbox del panel
        tarea.getChildren().remove(campo);
        tarea.getChildren().remove(ch);

        //Se pone en el label el texto extraido de campo, y se actualiza el checkbox
        texto.setText(info_tarea);
        ch.setSelected(check);
        
        //Se agregá el label con el texto recién extraido al panel
        tarea.getChildren().add(texto);

        if(texto.getText()!=""){ //Si es que el texto extraído se encuentra vacío, no se agrega al panel el checkbox.
            tarea.getChildren().add(ch);
            ch.setStyle("-fx-border-color: black;");
        }

        //Se elimina del JSON local, la información guardada de la instancia
        JSONTareas.remove("Tarea");
        JSONTareas.remove("Checkbox");

        //Se coloca en el JSON local, la información recientemente extraída
        JSONTareas.put("Tarea", info_tarea);
        JSONTareas.put("Checkbox", check);

        //Lo guardado en el JSON local, se coloca en el JSON global
        JSONG.put("Tarea" + id, JSONTareas);

    }

    //Metodo que se encarga de guardar la información del atributo ch.
    public void SaveCheck(){
        Boolean check = ch.isSelected();  //Se guarda en un boolean si es que el checkbox esta seleccionado

        JSONTareas.remove("Checkbox");  //Se elimina del Objeto JSONTareas, la información que había sido guardada del Checkbox

        JSONTareas.put("Checkbox", check);  //Se agrega al JSON la nueva información recientemente extraída

        JSON.remove("Tarea" + id);  //Se elimina del Objeto JSON, la información guardada de esta instancia.

        JSON.put("Tarea" + id, JSONTareas);  //Se agrega al Objeto JSON, la informacíon recientemente actualizada

    }

    //Metodo que es llamado cuando se presiona el botón editar en el menú individual de la Mascota
    public void Edit(){
        editmode = true;  //El modo edit se activa

        //Se extrae la información del label texto y del checkbox ch
        String info_tarea = texto.getText();
        Boolean check = ch.isSelected();

        //Se quita el label y el checkbox del panel
        tarea.getChildren().remove(texto);
        tarea.getChildren().remove(ch);

        //Se setea el campo de texto campo con el texto que tenía el label texto. Se setea nuevamente el checkbox con su valor.
        campo.setText(info_tarea);
        ch.setSelected(check);

        tarea.getChildren().add(campo);  //Se añade el campo de texto al panel
        if(texto.getText()!=""){  //Solo si el texto colocado en el campo de texto no esta vacío, también se coloca el checkbox en el panel
            tarea.getChildren().add(ch);
            ch.setStyle("-fx-border-color: black;");
        }
        
    }

    //Metodo que retorna el panel de la instancia.
    public HBox getTarea(){
        return tarea;
    }

    public HBox tarea;  //Panel que guardará y desplegará los atributos campo, ch y texto.
    public TextField campo;  //Campo de texto usado en el modo editar
    public CheckBox ch;  //Checkbox que podrá seleccionar el usuario para indicar si una tarea ha sido realizada o no
    public Label texto;  //Label que mostrará la Tarea ingresada.

    public static int id_dato=0; //Atributo estatico para generar id.
    public int id;  //Id de la instancia de Tarea.

    public JSONObject JSONTareas;  //Objeto JSON para guardar en formato JSON los datos de esta tarea.
    public JSONObject JSON;  //Objecto JSON que recibirá la tarea, en la que se agregará el objeto JSONTareas.

    public Boolean editmode; //Atributo que indica si se estan editando los datos de esta Tarea.
}

