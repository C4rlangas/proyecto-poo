import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import java.io.File;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;

public class MenuPrincipal{

    final File folder = new File("JSON");//se abre la carpeta que contiene los archivos .json que almacenan los datos
    ArrayList<Mascota> lista = new ArrayList<Mascota>();//se crea una lista que almacenara todas las mascotas al momento de ser creadas
    GridPane panel;

    public void listFilesForFolder(final File folder, ArrayList<Mascota> lista) {//funcion global que extrae los nombres de las mascotas 
        for (final File fileEntry : folder.listFiles()) {                        //almacenadas con anterioridad para incluirlas nuevamente
            if (fileEntry.isDirectory()) {                                       //en la lista de mascotas
                break;
            } else {
                String fname = fileEntry.getName();
                int pos = fname.lastIndexOf(".");
                if (pos > 0) {
                    fname = fname.substring(0, pos);
                }
                new Mascota(fname, lista, panel);
            }
        }
    }
    
    public MenuPrincipal(Stage primaryStage){//constructor de menu principal

        Image background = new Image("img/background.png");//se importa una imagen para su uso como background
        BackgroundImage backgroundImage = new BackgroundImage(//se setea la imagen de modo que se duplique para llenar la ventana
            background, 
            BackgroundRepeat.REPEAT, 
            BackgroundRepeat.REPEAT, 
            BackgroundPosition.DEFAULT, 
            BackgroundSize.DEFAULT
        );
        

        //se setea el ancho de las columnas del Pane y se incluye el fondo para el menu principal
        //---------------------------------------------------------//
        Background fondo = new Background(backgroundImage);
        panel = new GridPane();
        panel.setBackground(fondo);
        panel.setGridLinesVisible(false);
        panel.getColumnConstraints().add(new ColumnConstraints(100));
        panel.getColumnConstraints().add(new ColumnConstraints(100));
        panel.getColumnConstraints().add(new ColumnConstraints(100));
        panel.getColumnConstraints().add(new ColumnConstraints(100));
        panel.getColumnConstraints().add(new ColumnConstraints(100));
        panel.getColumnConstraints().add(new ColumnConstraints(100));
        panel.getColumnConstraints().add(new ColumnConstraints(100));
        panel.getColumnConstraints().add(new ColumnConstraints(100));
        panel.getColumnConstraints().add(new ColumnConstraints(100));
        //---------------------------------------------------------//

        listFilesForFolder(folder, lista);//funcion global que trae las mascotas creadas con anterioridad nuevamente a la lista

        for (Mascota mascota : lista) {//se recorre la lista de mascotas
            panel.add(mascota.getButton(), lista.indexOf(mascota)%8, (int)(lista.indexOf(mascota)/8), 1, 1);//se agregan los botones de las respectivas mascotas al menu principal
        }

        panel.setHgap(20);//se setea el espacio entre columnas del Pane
        panel.setVgap(20);//se setea el espacio entre filas del Pane
        panel.setPadding(new Insets(20));//se setea un borde por cada casilla del Pane

        Image img = new Image("img/add_button.png");  //Imagen utilizada para el botón 
        ImageView icon = new ImageView(img);  //Instancia de icon al cual se le agrega la imagen del boton

        //Personalización del icono
        icon.setFitHeight(25);
        icon.setPreserveRatio(true);
        icon.setRotate(0.0);

        //Creacion y personalizacion del botón que se utilizará para agregar Mascotas
        Button agregar = new Button();//se crea boton para agregar mascotas
        agregar.setGraphic(icon);//se le da un icono al boton de agregar
        agregar.setMaxWidth(Double.MAX_VALUE);
        agregar.setStyle("-fx-background-color: lightgreen; -fx-border-color: black;");//se rellena con color lo que no se cubre con el icono en el botoino agregar
        

        panel.add(agregar, 8, 0, 1, 1);

        agregar.setOnMouseClicked(e->{//handler que se ejecuta al clickear el boton "agregar"
            String nombre =JOptionPane.showInputDialog("Ingrese nombre de mascota");//se despliega una ventana que consulta por el nombre de la mascota quese desea agregar
            if(nombre!=null){//condicion para asegurar que no se guarde al presionar el boton cancelar del JOptionPane
                Boolean u = false;//variable local para identificar coincidencia
                for(Mascota k:lista){//se recorre la lista de mascotas buscando coincidencia en el nombre
                    if(nombre.equals(k.getName())){
                        u=true;
                    }
                }
                if(!u){//de no haber coincidencia en el nombre entonces se crea la nueva mascota
                    Mascota mascota = new Mascota(nombre,lista,panel);
                    panel.add(mascota.getButton(), lista.indexOf(mascota)%8, (int)(lista.indexOf(mascota)/8), 1, 1);//se añade el boton respectivo a la mascota en el menu principal
                }
            }
        });
        
        Scene scene = new Scene(panel, 1100, 600);//se crea la escena con las dimensiones
        primaryStage.setTitle("CaaPoo App"); //Se setea el nombre que tendra la ventana
        primaryStage.setScene(scene); //se setean la dimensiones de la ventana de menu principal
        primaryStage.setResizable(false);//se evita que se modifiquen las dimensiones del menu principal
        primaryStage.show(); // se despliega la ventana
    }  
}