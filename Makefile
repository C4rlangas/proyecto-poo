JC = javac
JM = java
MAIN = Main
RM = rm -f
CLASES = *.class
LIBPATH = C:\Program Files\Java\javafx-sdk-18.0.1\lib

CLASSES = \
Main.java \
Dato.java \
Mascota.java \
MenuIndividual.java \
MenuPrincipal.java \
Tarea.java

default: compile

compile:
	$(JC) --module-path "$(LIBPATH)" --add-modules ALL-MODULE-PATH $(MAIN).java

run:
	$(JM) --module-path "$(LIBPATH)" --add-modules ALL-MODULE-PATH $(MAIN)

clean:
	$(RM) $(CLASES)