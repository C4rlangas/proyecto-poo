import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.geometry.Insets;
import java.util.ArrayList;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.layout.ColumnConstraints;
import org.json.simple.JSONObject;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.image.Image;
import javafx.scene.text.Font;

public class MenuIndividual{

    public MenuIndividual(String info, int indice, ArrayList<Mascota> lista, Mascota mascota, GridPane panel){//constructor 

        Dato.id_dato = 0;//se resetean los id por cada mascota que se cree
        Tarea.id_dato = 0;//de Tareas como de Datos
        //-------------------------Set of Stage---------------------------
        secundaryStage = new Stage(); // Create a new stage
        secundaryStage.setTitle("Ficha " + info); // Se setea el nombre de la ventana 
        GridPane pane = new GridPane();//se setea el tipo de pane de la ventana 

        Image background = new Image("img/menu_individual.png");//se importa la imagen para el fondo
        BackgroundImage backgroundImage2 = new BackgroundImage(//se setean los detalles para agregar la imagen al fondo
            background, 
            BackgroundRepeat.REPEAT, 
            BackgroundRepeat.REPEAT, 
            BackgroundPosition.DEFAULT, 
            BackgroundSize.DEFAULT
        );

        Background fondo = new Background(backgroundImage2);
        pane.setBackground(fondo);//se setea la imagen del fondo
        pane.setHgap(10);//se setean los espacios entre casillas
        pane.setVgap(5);
        pane.setPadding(new Insets(5));
        //-------Se añaden columnas y filas de dimensiones definidas al Pane-------------
        ColumnConstraints columna1 = new ColumnConstraints();
        ColumnConstraints columna2 = new ColumnConstraints();
        RowConstraints row1 = new RowConstraints();
        RowConstraints row2 = new RowConstraints();
        RowConstraints row3 = new RowConstraints();
        RowConstraints row4 = new RowConstraints();
        RowConstraints row5 = new RowConstraints();
        RowConstraints row6 = new RowConstraints();
        RowConstraints row7 = new RowConstraints();
        RowConstraints row8 = new RowConstraints();
        RowConstraints row9 = new RowConstraints();
        RowConstraints row10 = new RowConstraints();
        RowConstraints row11 = new RowConstraints();
        RowConstraints row12 = new RowConstraints();
        columna1.setPrefWidth(250);
        columna2.setPrefWidth(250);
        row1.setPrefHeight(30);
        row2.setPrefHeight(30);
        row3.setPrefHeight(30);
        row4.setPrefHeight(30);
        row5.setPrefHeight(30);
        row6.setPrefHeight(30);
        row7.setPrefHeight(30);
        row8.setPrefHeight(30);
        row9.setPrefHeight(30);
        row10.setPrefHeight(30);
        row11.setPrefHeight(30);
        row12.setPrefHeight(30);
        pane.getColumnConstraints().add(columna1);
        pane.getColumnConstraints().add(columna2);
        pane.getRowConstraints().add(row1);
        pane.getRowConstraints().add(row2);
        pane.getRowConstraints().add(row3);
        pane.getRowConstraints().add(row4);
        pane.getRowConstraints().add(row5);
        pane.getRowConstraints().add(row6);
        pane.getRowConstraints().add(row7);
        pane.getRowConstraints().add(row8);
        pane.getRowConstraints().add(row9);
        pane.getRowConstraints().add(row10);
        pane.getRowConstraints().add(row11);
        pane.getRowConstraints().add(row12);
        //------------------------------------------------

        Label tareas = new Label("Tareas");//Titulo para las Tareas junto con sus configuraciones visuales
        tareas.setStyle("-fx-font-weight: bold");
        tareas.setFont(new Font("Comic Sans MS", 22));

        Label datos = new Label("Datos");//Titulo para los Datos junto con sus configuraciones visuales
        datos.setStyle("-fx-font-weight: bold");
        datos.setFont(new Font("Comic Sans MS", 22));

        Button editar = new Button("Editar");//Boton para entrar en modo editar campos para ingreso de texto) y sus configuraciones visuales
        editar.setStyle("-fx-background-color: orange; -fx-border-color: black;");
        editar.setFont(new Font("Comic Sans MS", 14));
        editar.setMaxWidth(Double.MAX_VALUE);

        Button guardar = new Button("Guardar");//Boton para entrar en modo estatico (texto fijo) y sus configuraciones visuales
        guardar.setStyle("-fx-background-color: orange; -fx-border-color: black;");
        guardar.setFont(new Font("Comic Sans MS", 14));
        guardar.setMaxWidth(Double.MAX_VALUE);

        Button eliminar = new Button("Eliminar");//Boton para eliminar a la mascota de la respectiva ficha y sus configuraciones visuales
        eliminar.setStyle("-fx-background-color: orange; -fx-border-color: black;");
        eliminar.setFont(new Font("Comic Sans MS", 14));
        eliminar.setMaxWidth(Double.MAX_VALUE);

        pane.add(datos,0,0,1,1);//Titulo Datos
        pane.add(tareas,1,0,1,1);//Titulo tareas
        //------------------------------------------------------------

        ArrayList<Dato> listaD= new ArrayList<Dato>();
        ArrayList<Tarea> listaT= new ArrayList<Tarea>();

        JSONDatos = new JSONObject();

        pane.add(eliminar, 1, 11, 1, 1); // Botón para eliminar el botón mascota

        eliminar.setOnMouseClicked(e->{
            deleteButton(indice, lista, mascota,panel,info);
        });
        
        //----------------------Chequea existencia de archivo y si este esta vacío o no---------------------

        Boolean vacio = true;  //Booleano que se modificará de acuerdo a si el .json de la Mascota se encuentra vacío o no.

        File json = new File("./JSON/" + info + ".json");  //Se abre el .json correspondiente a la Mascota.
         
        if(json.exists() && !json.isDirectory()) { //En caso de que exista, chequeara si es que tiene información o no

            if(json.length()!=0)  //Si el .json tiene datos
            {
                vacio = false;  //vacio se setea a falso. Este no estaba vacío.
                pane.add(editar, 0, 11, 1, 1);  //Se agrega al panel de esta instancia solo el botón editar
            }
            else   //Si el .json no tiene datos
            {
                pane.add(guardar, 0, 11, 1, 1);  //Se agrega al panel de esta instancia solo el boton guardar
            }
        }
        else  //En caso de que no exista el archivo .json buscado
        {
            try {
                json.createNewFile();  //Se crea el archivo
            } catch (Exception e) {
                System.out.println("Error");
            }
            pane.add(guardar, 0, 11, 1, 1);  //Se agrega unicamente el boton guardar al panel de esta instancia
        }
        //------------------------------------------------------------------------------------------------

        //------------------------------Lógica para Datos--------------------------
        new Dato("Nombre",listaD,vacio,info,JSONDatos);//Se agregan los datos que contendra la ficha
        new Dato("Edad",listaD,vacio,info,JSONDatos);
        new Dato("Especie",listaD,vacio,info,JSONDatos);
        new Dato("Alergias",listaD,vacio,info,JSONDatos);
        new Dato("Vacunas",listaD,vacio,info,JSONDatos);
        new Dato("Chip",listaD,vacio,info,JSONDatos);

        for(Dato k:listaD){//Añadir datos al pane
            pane.add(k.getData(), 0, k.getID(), 1, 1);
        }
        
        editar.setOnMouseClicked(e->{//handler que realizar el switch entre los botones editar y guardar ademas de activar el modo de edicion
            pane.getChildren().remove(editar);
            pane.add(guardar, 0, 11, 1, 1);

            for(Dato d:listaD) d.Edit();
            for(Tarea t:listaT) t.Edit();
        });

        guardar.setOnMouseClicked(e -> {//handler que realizar el switch entre los botones editar y guardar ademas de activar el modo estatico
            pane.getChildren().remove(guardar);
            pane.add(editar, 0, 11, 1, 1);

            for(Dato d:listaD) d.Save(JSONDatos);
            for(Tarea t:listaT) t.Save(JSONDatos);
            
            try (FileWriter archivo = new FileWriter("./JSON/" + info + ".json")) {//se realizan los cambios guardados en los archivos .json
                archivo.write(JSONDatos.toJSONString()); 
                archivo.flush();
        
            } catch (IOException eve) {
                eve.printStackTrace();
            }
        });

        //------------------------------------------------------------

        //------------------------Logica para Tareas--------------------
        Tarea tarea1= new Tarea(vacio,info,listaT,JSONDatos);//Se agregan tareas a la ficha
        Tarea tarea2= new Tarea(vacio,info,listaT,JSONDatos);
        Tarea tarea3= new Tarea(vacio,info,listaT,JSONDatos);
        Tarea tarea4= new Tarea(vacio,info,listaT,JSONDatos);
        Tarea tarea5= new Tarea(vacio,info,listaT,JSONDatos);
        Tarea tarea6= new Tarea(vacio,info,listaT,JSONDatos);
        Tarea tarea7= new Tarea(vacio,info,listaT,JSONDatos);
        Tarea tarea8= new Tarea(vacio,info,listaT,JSONDatos);
        Tarea tarea9= new Tarea(vacio,info,listaT,JSONDatos);
        Tarea tarea10= new Tarea(vacio,info,listaT,JSONDatos);

        pane.add(tarea1.getTarea(),1,1,1,1);
        pane.add(tarea2.getTarea(),1,2,1,1);
        pane.add(tarea3.getTarea(),1,3,1,1);
        pane.add(tarea4.getTarea(),1,4,1,1);
        pane.add(tarea5.getTarea(),1,5,1,1);
        pane.add(tarea6.getTarea(),1,6,1,1);
        pane.add(tarea7.getTarea(),1,7,1,1);
        pane.add(tarea8.getTarea(),1,8,1,1);
        pane.add(tarea9.getTarea(),1,9,1,1);
        pane.add(tarea10.getTarea(),1,10,1,1);
        //-------------------------------------------------------------

        secundaryStage.setScene(new Scene(pane, 500, 410));
        secundaryStage.setResizable(false);
        secundaryStage.getIcons().add(new Image("img/icon.png"));
    }

    public void show(){
        secundaryStage.show();// Display the stage 
    }

    void deleteButton(int indice, ArrayList<Mascota> lista, Mascota mascota, GridPane panel, String info){//metodo para eliminar mascotas
        secundaryStage.close();//se cierra el menu individual correspondiente a la mascota
        for (Mascota k : lista) {//se quitan todos los botones del menu principal
            panel.getChildren().remove(k.getButton());
        }
        lista.remove(mascota);//se quita a la mascota que se desea eliminar de la lista

        File myObj = new File("./JSON/" + info + ".json"); 
        myObj.delete();//se elimina el archivo json de la mascota a eliminar
        
        for (Mascota k : lista) {//se vuelven añadir los botones de mascota al menu principal, esto para evitar "huecos"
            panel.add(k.getButton(), lista.indexOf(k)%8, (int)(lista.indexOf(k)/8), 1, 1);
        }
    }

    Stage secundaryStage;
    JSONObject JSONDatos;
}
