import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {  
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
       
        primaryStage.getIcons().add(new Image("img/icon.png"));//se añade un icono a la aplicacion
        new MenuPrincipal(primaryStage);//se crea el menu principal
        
    }
}