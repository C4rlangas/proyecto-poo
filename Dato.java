import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import java.util.ArrayList;
import javafx.scene.text.Font;

import org.json.simple.parser.JSONParser;
import java.io.FileReader;
import org.json.simple.JSONObject;

public class Dato {
    
    public Dato(String dato, ArrayList<Dato> lista, Boolean vacio, String info, JSONObject JSON){//constructor

        //Se crea una un label y se edita esteticamente
        Label texto = new Label(dato + ": ");
        texto.setStyle("-fx-font-weight: bold");
        texto.setFont(new Font("Comic Sans MS", 20));
        texto.setPrefWidth(100);

        this.dato = dato;  //Se guarda la variable recibida dato, correspondiente al dato que guardará esta instancia
        
        id_dato ++;  //Se aumenta en 1 el id estático de la clase
        id = id_dato;  //Luego, este id se asigna a esta instancia en específico
        
        if(vacio){  //En caso de que la Mascota no tenga datos

            //Se crean instancias de campo y texto2.
            campo = new TextField();
            texto2 = new Label();
            texto2.setFont(new Font("Comic Sans MS", 20));

            //Se crea instancia del panel datos, en el cual, se añaden campo y texto2
            datos = new HBox(texto, campo);
            
        }
        else{  //Si la Mascota ya cuenta con información guardada
            JSONParser jsonParser = new JSONParser();

            try (FileReader reader = new FileReader("./JSON/" + info + ".json")) {
                
                Object obj = jsonParser.parse(reader);  //Se extrae del .json de la Mascota en particular, su información.

                JSONObject PetObject = (JSONObject) obj;


                String info_dato = (String) PetObject.get(dato);  //Se guarda la informacion del dato extraída en una variable

                //Se crea y personaliza una instancia de Label.
                texto2 = new Label(info_dato);
                texto2.setFont(new Font("Comic Sans MS", 20));
                
                //Se crea una instancia del panel datos, en la cual se añade texto(Con el tipo de dato) y texto2 (Con la información acerca del dato)
                datos= new HBox(texto,texto2);

                //Se crea una instancia del atributo campo
                campo = new TextField();//variable
                
            } catch (Exception e) {
                System.out.println("Error");
            }
        }

        lista.add(this);
    }

    //Metodo que es llamado cuando se presiona el botón Guardar en el menú individual de la Mascota
    public void Save(JSONObject JSON){

        String data = campo.getText();  //Se extrae la información escrita en el campo de texto.
        datos.getChildren().remove(campo);  //Se quita del panel el campo de texto

        texto2.setText(data);  //Se escribe en el label texto2 la información recien extraida.
        datos.getChildren().add(texto2);  //Se agrega el label texto 2 al panel.

        JSON.remove(dato); //Del JSON global, se quita la información que este tenia acerca de esta instancia de Dato
        JSON.put(dato,data); // Se agrega al JSON global la informacion recien extraida.
    }

    //Metodo que es llamado cuando se presiona el botón editar en el menú individual de la Mascota
    public void Edit(){

        String data = texto2.getText();  //Se extrae la información contenida por el label texto2
        datos.getChildren().remove(texto2);  //Se quita del panel el label texto2

        campo.setText(data);  //Se escribe en el campo de texto la informacion recien extaida
        datos.getChildren().add(campo);  //Se añade el campo de texto al panel
    }

    //Metodo que retorna el panel de la instancia.
    public HBox getData(){
        return datos;
    }

    //Funcion que retorna el id de la instancia
    public int getID(){
        return id;
    }

    public HBox datos;  //Panel que guardará y desplegará los atributos campo, texto y texto2

    public TextField campo;  //Campo de texto en el cual el usuario escribirá
    public Label texto2;  //Label que guardará y mostrará la información escrita en campo.

    public static int id_dato = 0;  //Atributo estatico para generar id.
    public int id;  //Id de la instancia de Dato.

    public String dato;  //String que contendra el tipo de dato a guardar por la instancia.
}
