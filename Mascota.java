import java.util.ArrayList;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

public class Mascota {
        public Mascota(String nombre, ArrayList<Mascota> lista, GridPane panel){//constructor de Mascota
        name=nombre;//se setea atributo nombre
        id_local = id_global; //id local se copia de id_global
        id_global ++;   //se aumenta el id gloabl de la clase
        boton = new Button(nombre);//se crea el boton para la mascota con el nombre de la respectiva

        //--------------detalles esteticos del boton---------------//
        boton.setStyle("-fx-background-color: lightyellow; -fx-border-color: black;");
        boton.setFont(new Font("Comic Sans MS", 20));
        boton.setMaxWidth(Double.MAX_VALUE);
        boton.setWrapText(true);
        boton.setTextAlignment(TextAlignment.CENTER);
        //---------------------------------------------------------//
        
        ficha = new MenuIndividual(nombre, lista.indexOf(this), lista, this, panel);//se crea el menu individual para la respectiva mascota
        lista.add(this);//se añade la mascota a una lista de mascotas
        boton.setOnMouseClicked(e->ficha.show());//handler que despliega el menu individual de cada mascota al ser preionado su respectivo boton
    }

    public Button getButton(){//metodo para obtener el boton
        return boton;
    }

    public int getID (){//metodo para obtener el id de la mascota respectiva
        return id_local;
    }
    public String getName(){//metodo para obtener el nombre de la mascota respectiva
        return name;
    }
    Button boton;//se declara el atributo del tipo boton
    MenuIndividual ficha;// se declarael atributo ficha del tipo MenuIndividual
    static int id_global=0;//atributo estatico, se comparte por todas las instancias de mascota
    int id_local;//atributo id
    String name;//atributo nombre

}
